from django.db import models
from django.db.models.fields import related
from django.db.models.fields.related import ForeignKey
from .User import idUser
from .Post import idPost

class Interest(models.model):
    idInterest = models.IntegerField()
    idUserFK = models.ForeingKey(User ,related_name= idUserFK , on_delete=models.CASCADE) 
    idPostFK = models.ForeignKey(Post ,related_name= idPostFK , on_delete=models.CASCADE)
    interestDate = models.DateField()


